package ru.kyune.intellect.client;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.UUID;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import ru.kyune.intellect.api.enums.ResponseStatus;
import ru.kyune.intellect.api.json.request.GUIDRequest;
import ru.kyune.intellect.api.json.request.GetAnswersRequest;
import ru.kyune.intellect.api.json.request.GetTurnRequest;
import ru.kyune.intellect.api.json.request.MakeAnswerRequest;
import ru.kyune.intellect.api.json.request.MakeTurnRequest;
import ru.kyune.intellect.api.json.response.GetAnswersResponse;
import ru.kyune.intellect.api.json.response.GetGameResultResponse;
import ru.kyune.intellect.api.json.response.GetGameStateResponse;
import ru.kyune.intellect.api.json.response.GetMapResponse;
import ru.kyune.intellect.api.json.response.GetQuestionResponse;
import ru.kyune.intellect.api.json.response.GetTurnResponse;
import ru.kyune.intellect.api.json.response.ResponseGeneric;
import ru.kyune.intellect.api.json.response.StartGameResponse;

import com.google.gson.Gson;

public class Connector {
	
	private Gson gsonParser = new Gson();	
	private String GUID = UUID.randomUUID().toString();	
	private String BASE_URL;	

	public Connector(String base_url) {		
		BASE_URL = base_url;
	}


	public StartGameResponse startGame() throws UnsupportedEncodingException, ClientProtocolException, IOException, InterruptedException {
		String methodURL = "startGame";
		GUIDRequest request = new GUIDRequest();
		request.GUID = GUID;
		String requestString = gsonParser.toJson(request);	
		while (true) {
			String responseString = request(requestString, methodURL);
			StartGameResponse response = gsonParser.fromJson(responseString, StartGameResponse.class);
			ResponseStatus status = ResponseStatus.fromString(response.status);
			switch (status) {
			case OK:
				return response;
			case ERROR:
				System.out.println(response.message);
				Thread.sleep(2000);
				break;
			case RETRY: 
			default:
				System.out.println("waiting for other players...");
				Thread.sleep(2000);
				break;
			}
		}
	}



	private String request(String requestString, String methodURL) 
			throws UnsupportedEncodingException,
			IOException, ClientProtocolException {		
		String responseString = null;
		
		HttpPost httppost = new HttpPost(BASE_URL + methodURL);
		StringEntity stringEntity = new StringEntity(requestString);		
		stringEntity.setContentType("application/json");
		httppost.setEntity(stringEntity);			 
		HttpClient httpclient = new DefaultHttpClient();
		HttpResponse response = httpclient.execute(httppost);
		HttpEntity resEntity = response.getEntity();
		if (resEntity != null) {	             
		    responseString = EntityUtils.toString(resEntity);
		}
		EntityUtils.consume(resEntity);
		 
		return responseString;
	}



	public GetMapResponse getMap() {
		GetMapResponse response = null;
		try {
			String methodURL = "getMap";
			GUIDRequest request = new GUIDRequest();
			request.GUID = GUID;
			
			String requestString = gsonParser.toJson(request);	

			String responseString = request(requestString, methodURL);
			response = gsonParser.fromJson(responseString, GetMapResponse.class);			

			 	
		} catch (Exception e) {			
			e.printStackTrace();
		}		
		return response;
	}

	public ResponseGeneric makeTurn(String x, String y) throws Exception {
		ResponseGeneric response = null;

		String methodURL = "makeTurn";
		MakeTurnRequest request = new MakeTurnRequest();
		request.x = Integer.valueOf(x);
		request.y = Integer.valueOf(y);
		request.GUID = GUID;
		
		String requestString = gsonParser.toJson(request);	
		String responseString = request(requestString, methodURL);
		response = gsonParser.fromJson(responseString, ResponseGeneric.class);
		return response;
	}


	public GetTurnResponse getTurn(int turn) {
		GetTurnResponse response = null;
		try {
			String methodURL = "getTurn";
			GetTurnRequest request = new GetTurnRequest();
			request.GUID = GUID;
			request.turnNumber = turn;
			
			String requestString = gsonParser.toJson(request);	

			String responseString = request(requestString, methodURL);
			response = gsonParser.fromJson(responseString, GetTurnResponse.class);			
 	
		} catch (Exception e) {			
			e.printStackTrace();
		}		
		return response;
	}



	public GetQuestionResponse getQuestion() {
		GetQuestionResponse response = null;
		try {
			String methodURL = "getQuestion";
			GUIDRequest request = new GUIDRequest();
			request.GUID = GUID;
			
			String requestString = gsonParser.toJson(request);	
			String responseString = request(requestString, methodURL);
			response = gsonParser.fromJson(responseString, GetQuestionResponse.class);				
			 	
		} catch (Exception e) {			
			e.printStackTrace();
		}		
		return response;
	}



	public ResponseGeneric makeAnswer(String answer) throws Exception {
		ResponseGeneric response = null;
		String methodURL = "answer";
		MakeAnswerRequest request = new MakeAnswerRequest();
		request.answer = answer.toUpperCase();
		request.GUID = GUID;
		
		String requestString = gsonParser.toJson(request);	
		String responseString = request(requestString, methodURL);
		response = gsonParser.fromJson(responseString, ResponseGeneric.class);
		return response;
	}



	public GetAnswersResponse getAnswers(int turnNumber, int qNumber) {
		GetAnswersResponse response = null;
		try {
			String methodURL = "getAnswers";
			GetAnswersRequest request = new GetAnswersRequest();
			request.GUID = GUID;
			request.qNumber = qNumber;
			request.turnNumber = turnNumber;
			
			String requestString = gsonParser.toJson(request);	

			String responseString = request(requestString, methodURL);
			response = gsonParser.fromJson(responseString, GetAnswersResponse.class);
	 	
		} catch (Exception e) {			
			e.printStackTrace();
		}		
		return response;
	}


	public GetGameStateResponse getGameState() {
		GetGameStateResponse response = null;
		try {
			String methodURL = "getGameState";
			GUIDRequest request = new GUIDRequest();
			request.GUID = GUID;
			
			String requestString = gsonParser.toJson(request);	
			String responseString = request(requestString, methodURL);
			response = gsonParser.fromJson(responseString, GetGameStateResponse.class);			
			 	
		} catch (Exception e) {			
			e.printStackTrace();
		}		
		return response;
	}


	public GetGameResultResponse getGameResult() {
		GetGameResultResponse response = null;
		try {
			String methodURL = "getGameResult";
			GUIDRequest request = new GUIDRequest();
			request.GUID = GUID;
			
			String requestString = gsonParser.toJson(request);	
			String responseString = request(requestString, methodURL);
			response = gsonParser.fromJson(responseString, GetGameResultResponse.class);			
			 	
		} catch (Exception e) {			
			e.printStackTrace();
		}		
		return response;
	}



}
