package ru.kyune.intellect.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

import ru.kyune.intellect.api.enums.GameState;
import ru.kyune.intellect.api.enums.ResponseStatus;
import ru.kyune.intellect.api.json.response.GetAnswersResponse;
import ru.kyune.intellect.api.json.response.GetGameResultResponse;
import ru.kyune.intellect.api.json.response.GetGameStateResponse;
import ru.kyune.intellect.api.json.response.GetMapResponse;
import ru.kyune.intellect.api.json.response.GetQuestionResponse;
import ru.kyune.intellect.api.json.response.GetTurnResponse;
import ru.kyune.intellect.api.json.response.ResponseGeneric;
import ru.kyune.intellect.api.json.response.StartGameResponse;

public class Starter {
	
	public static void main(String[] args) {	
		Starter s = new Starter();
		try {
			s.play(args[0]);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private String map;
	private int UID;
	private Command curCommand = Command.EnterGame;
	private Connector connector;
	
	private int turnNumber = 0;
	private int qNumber = 0;
	
	private void play(String URL) throws InterruptedException {
		connector = new Connector(URL);
		
		main:
		while (true) {
			switch (curCommand) {
			case EnterGame:
				System.out.println("Type 'e' to start New game: ");				
				String action = getAction();	
				if (action.equals("e")) {
					StartGameResponse response = null;
					try {
						response = connector.startGame();
					} catch (Exception e) {
						e.printStackTrace();
						break main;
					} 
					UID = response.UID;
					System.out.println("Your name: " + UID);
					curCommand = Command.GetMap;
				}				
				break;
			case GetMap:
			{
				GetMapResponse resp = connector.getMap();
				ResponseStatus status = ResponseStatus.fromString(resp.status);
				switch (status) {
				case ERROR:
					System.out.println(resp.message);
					Thread.sleep(2000);
					break;
				case OK:
					map = resp.map;
					turnNumber = resp.turnNumber;
					drawMap(resp.map);
					System.out.println("You have " + resp.timeLeft + "to make a move");
					if (resp.whosTurn == UID) {
						curCommand = Command.MakeTurn;
					} else {
						curCommand = Command.GetTurn;
					}
					break;
				case OUT_OF_DATE:
					curCommand = Command.GetGameState;
					break;
				case GAME_OVER:
					curCommand = Command.GetGameResult;
					break;
				case RETRY:
				case INVALID:
					curCommand = Command.GetGameState;
					break;
				}
			}
			break;
			case MakeTurn:
			{
				System.out.println("Make your move(x):");
				String x = getAction();				
				System.out.println("Make your move(y):");
				String y = getAction();
				try {
					ResponseGeneric resp = connector.makeTurn(x, y);
					ResponseStatus status = ResponseStatus.fromString(resp.status);
					switch (status) {
					case ERROR:
						System.out.println(resp.message);
						Thread.sleep(2000);
						break;
					case OK:
						int pos = (Integer.valueOf(x) - 1)*4 + Integer.valueOf(y) - 1;					
						if (String.valueOf(UID).equals(map.substring(pos, pos + 1))) {
							curCommand = Command.GetMap;
						} else {
							curCommand = Command.GetQuestion;
						}	
						break;
					case OUT_OF_DATE:
					case INVALID:
						curCommand = Command.GetGameState;
						break;
					case GAME_OVER:
						curCommand = Command.GetGameResult;
						break;
					case RETRY:
						curCommand = Command.GetGameState;
						break;
					}
				} catch (Exception e) {
					continue;
				}				
			}				
			break;
			case GetTurn:
			{				
				GetTurnResponse resp = connector.getTurn(turnNumber);		
				ResponseStatus status = ResponseStatus.fromString(resp.status);
				switch (status) {
				case ERROR:
					System.out.println(resp.message);
					Thread.sleep(2000);
					break;
				case OK:
					System.out.println("Player " + resp.uid + " goes to X:" + resp.x + " Y:" + resp.y);
					int pos = (resp.x - 1)*4 + resp.y - 1;				
					if (String.valueOf(resp.uid).equals(map.substring(pos, pos + 1))) {
						curCommand = Command.GetMap;
					} else {
						curCommand = Command.GetQuestion;
					}	
					break;
				case OUT_OF_DATE:
				case INVALID:
					curCommand = Command.GetGameState;
					break;
				case GAME_OVER:
					curCommand = Command.GetGameResult;
					break;
				case RETRY:
					Thread.sleep(1000);
				}				
			}				
			break;			
			case GetQuestion:
			{				
				GetQuestionResponse resp = connector.getQuestion();		
				ResponseStatus status = ResponseStatus.fromString(resp.status);
				switch (status) {
				case ERROR:
					System.out.println(resp.message);
					Thread.sleep(2000);
					break;
				case OK:
					try {
						qNumber = resp.qNumber;
						PrintStream ps = new PrintStream(System.out, true, "UTF-8");
						System.out.println("You have " + resp.timeLeft + "to answer Q");
						ps.println(resp.text);
						ps.println("A: " + resp.A);
						ps.println("B: " + resp.B);
						ps.println("C: " + resp.C);
						ps.println("D: " + resp.D);
						if (resp.needAnswer.equals("true")){
							curCommand = Command.MakeAnswer;
						} else {
							curCommand = Command.GetAnswers;
						}				
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
					break;
				case OUT_OF_DATE:
				case INVALID:
					curCommand = Command.GetGameState;
					break;
				case GAME_OVER:
					curCommand = Command.GetGameResult;
					break;
				case RETRY:
					Thread.sleep(1000);
					break;
				}	
				
			}		
			break;	
			case MakeAnswer:
			{				
				System.out.println("Enter your answer:");
				String answer = getAction();
				try {
					ResponseGeneric resp = connector.makeAnswer(answer);
					ResponseStatus status = ResponseStatus.fromString(resp.status);
					switch (status) {
					case ERROR:
						System.out.println(resp.message);
						Thread.sleep(2000);
						break;
					case OK:
						curCommand = Command.GetAnswers;
						break;
					case OUT_OF_DATE:
					case INVALID:
						curCommand = Command.GetGameState;
						break;
					case GAME_OVER:
						curCommand = Command.GetGameResult;
						break;
					case RETRY:
						curCommand = Command.GetGameState;
						break;
					}	

				} catch (Exception e){
					continue;
				}
			}		
			break;
			case GetAnswers:
			{				
				GetAnswersResponse resp = connector.getAnswers(turnNumber, qNumber);
				ResponseStatus status = ResponseStatus.fromString(resp.status);
				switch (status) {
				case ERROR:
					System.out.println(resp.message);
					Thread.sleep(2000);
					break;
				case OK:
					if (!resp.user1.equals("none")){
						if (UID != 1) {
							System.out.println("user 1 answered: " + resp.user1);
						}
					}
					
					if (!resp.user2.equals("none")){
						if (UID != 2) {
							System.out.println("user 2 answered: " + resp.user2);
						}
					}
					
					if (!resp.user3.equals("none")){
						if (UID != 3) {
							System.out.println("user 3 answered: " + resp.user3);
						}
					}	
					
					System.out.println("correct answer: " + resp.correct);
					
					if (resp.winner.equals("none")) {
						System.out.println("none");
						curCommand = Command.GetMap;
					} else if (resp.winner.equals("retry")) {
						System.out.println("One more question");
						curCommand = Command.GetQuestion;
					} else {
						System.out.println("Territory conquered !");
						curCommand = Command.GetMap;
					}				
					break;
				case OUT_OF_DATE:
				case INVALID:
					curCommand = Command.GetGameState;
					break;
				case GAME_OVER:
					curCommand = Command.GetGameResult;
					break;
				case RETRY:
					Thread.sleep(1000);
					break;
				}	
			}		
			break;
			case GetGameResult:
			{
				GetGameResultResponse resp = connector.getGameResult();
				ResponseStatus status = ResponseStatus.fromString(resp.status);
				switch (status) {
				case ERROR:
					System.out.println(resp.message);
					Thread.sleep(2000);
					break;
				case OK:
					System.out.println("The winner is "+ resp.winner);
					break main;
				case OUT_OF_DATE:
				case INVALID:
					curCommand = Command.GetGameState;
					break;
				case GAME_OVER:
					Thread.sleep(2000);
					break;
				case RETRY:
					curCommand = Command.GetGameState;
					break;
				}	
				break;				
			}	
			case GetGameState:
			{
				GetGameStateResponse resp = connector.getGameState();
				ResponseStatus status = ResponseStatus.fromString(resp.status);
				switch (status) {
				case ERROR:
					System.out.println(resp.message);
					Thread.sleep(2000);
					break;
				case OK:
					GameState state = GameState.fromString(resp.state);
					if (state == GameState.QUESTION) {
						curCommand = Command.GetQuestion;
					} else {
						curCommand = Command.GetMap;
					}
					break;
				case OUT_OF_DATE:
					Thread.sleep(2000);
					break;
				case GAME_OVER:
					curCommand = Command.GetGameResult;
					break;
				case RETRY:
					Thread.sleep(1000);
				default:
					break;
				}	
				break;
			}
			}
		}	
	}
	
	private void drawMap(String map) {
		System.out.println(map.substring(0, 4));
		System.out.println(map.substring(4, 8));
		System.out.println(map.substring(8, 12));
		System.out.println(map.substring(12, 16));		
	}

	private String getAction() {		
		try {
		    BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
		    String s = bufferRead.readLine();	 
		    return s;
		}
		catch(IOException e) {
			e.printStackTrace();
			return "";
		}		
	}

	enum Command {
		EnterGame,
		GetMap,
		MakeTurn,
		GetTurn,
		GetQuestion,
		MakeAnswer,
		GetAnswers,
		GetGameState,
		GetGameResult		
	}

}
