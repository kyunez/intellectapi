package ru.kyune.intellect.env;

import java.util.Collection;
import java.util.Set;

import org.apache.log4j.Logger;

import ru.kyune.intellect.common.PropertyManager;
import ru.kyune.intellect.engine.GamesPool;
import ru.kyune.intellect.engine.Game;

public class GarbageDaemon implements Runnable {
	
	private Logger log = Logger.getLogger(this.getClass()); 

	private GamesPool pool;
	
	public GarbageDaemon(GamesPool pool){
		this.pool = pool;
	}
	
	@Override
	public void run() {
		while (true) {
			int delay = PropertyManager.getGarbageDaemonDelay();
			try {
				Thread.sleep(delay);
				cleanGamesPool();
			} catch (InterruptedException e) {
				log.error(e);
			}
		}
	}

	private void cleanGamesPool() {
		log.info("Cleaning games pool - START");
		
		Collection<Game> games = pool.getAllGames();
		for (Game game : games) {
			if (game.isOutOfDate()) {
				game.forceFinish();
				Set<String> GUIDs = game.getAllUserGUIDs();
				for (String GUID : GUIDs) {
					pool.removeGameMapping(GUID);
				}
			}
		}
		
		
		log.info("Cleaning games pool - END");
	}

}
