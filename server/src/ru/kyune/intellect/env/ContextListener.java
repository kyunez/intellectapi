package ru.kyune.intellect.env;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;

import ru.kyune.intellect.common.ContextAttributes;
import ru.kyune.intellect.common.PropertyManager;
import ru.kyune.intellect.engine.GamesPool;


@WebListener
public class ContextListener implements ServletContextListener {
	
	private Logger log = Logger.getLogger(ContextListener.class);

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void contextInitialized(ServletContextEvent event) {
		initGlobalAppConfiguration(event);
		
		GamesPool pool = initGamesPool(event);
		
		initGarbageDaemon(pool);	
	}

	private void initGarbageDaemon(GamesPool pool) {
		GarbageDaemon daemon = new GarbageDaemon(pool);
		Thread daemonThread = new Thread(daemon);
		daemonThread.setDaemon(true);
		daemonThread.start();
	}

	private GamesPool initGamesPool(ServletContextEvent event) {
		ServletContext context = event.getServletContext();
		GamesPool pool = new GamesPool();
		context.setAttribute(ContextAttributes.GAMES_POOL, pool);
		return pool;
	}

	private void initGlobalAppConfiguration(ServletContextEvent event) {
		ServletContext context = event.getServletContext();
		try {
			PropertyManager.init(context);
		} catch (IOException e) {
			log.error(e);
		}
	}

	
}
