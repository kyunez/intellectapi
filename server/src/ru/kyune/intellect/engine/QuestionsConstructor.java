package ru.kyune.intellect.engine;

import java.util.HashMap;

import org.apache.log4j.Logger;

import ru.kyune.intellect.exc.IntellectException;

public class QuestionsConstructor {
	private static Logger log = Logger.getLogger(QuestionsConstructor.class);
	
	private static HashMap<String, Integer> answersStringsToNums = new HashMap<String, Integer>();
	static {
		answersStringsToNums.put("A", 1);
		answersStringsToNums.put("B", 2);
		answersStringsToNums.put("C", 3);
		answersStringsToNums.put("D", 4);
	}
	public static  int answerFromString(String a) throws IntellectException{
		Integer ai = answersStringsToNums.get(a);
		if (ai == null) {
			log.error("Cannot map " + a + "to integer");
			throw new IntellectException("Cannot map " + a + "to integer");
		} 
		return ai;
	}
	
	private static HashMap<Integer, String> answersNumsToStrings = new HashMap<Integer, String>();
	static {
		answersNumsToStrings.put(1, "A");
		answersNumsToStrings.put(2, "B");
		answersNumsToStrings.put(3, "C");
		answersNumsToStrings.put(4, "D");
	}
	public static  String answerFromInt(Integer a) throws IntellectException {
		if (a == null) {
			return "none";
		}
		String as = answersNumsToStrings.get(a);
		if (as == null) {
			log.error("Cannot map " + a + "to String");
			throw new IntellectException("Cannot map " + a + "to String");
		} 
		return as;
	}
}
