package ru.kyune.intellect.engine;

import java.util.HashMap;
import java.util.Map;

import ru.kyune.intellect.action.Question;
import ru.kyune.intellect.common.PropertyManager;
import ru.kyune.intellect.data.QuestionDAO;
import ru.kyune.intellect.exc.IntellectException;

public class QuestionManager {
	private static final int playersNumber = PropertyManager.getPlayersNumber();
	
	private QuestionDAO questionDAO = new QuestionDAO();
	
	private int currentTurn = 0;
	private int currentQuestion = 0;
	
	/**
	 * turnNumber -> qNumber -> Question
	 */
	private HashMap<Integer, Map<Integer, Question>> questionsByTurn = 
			new HashMap<Integer, Map<Integer, Question>>(); 
	
	/**
	 * turnNumber -> qNumber -> UID -> answer
	 */
	private HashMap<Integer, Map<Integer, Map<Integer, Integer>>> answersByTurn = 
			new HashMap<Integer, Map<Integer, Map<Integer, Integer>>>(); 
	
	/**
	 * turnNumber -> winner 
	 */
	private HashMap<Integer, Integer> winnersByTurn = new HashMap<Integer, Integer>();
	
	private HashMap<Integer, Integer> attackPlayerByTurn = new HashMap<Integer, Integer>();
	private HashMap<Integer, Integer> deffencePlayerByTurn = new HashMap<Integer, Integer>();			
	
	public Question getCurrentQuestion() {
		return questionsByTurn.get(currentTurn).get(currentQuestion);
	}
	
	public int getAttackPlayer() {
		return attackPlayerByTurn.get(currentTurn);
	}
	
	public int getDeffencePlayer() {
		return deffencePlayerByTurn.get(currentTurn);
	}
	
	public void makeAnswer(String answer, int UID) throws IntellectException {
		Map<Integer, Integer> answers = answersByTurn.get(currentTurn).get(currentQuestion);
		if (answers.get(UID) != null) {
			throw new IntellectException("You have already answered to turn #" 
					+ currentTurn + " q #" + currentQuestion);
		}
		
		int  ans = QuestionsConstructor.answerFromString(answer);
		answers.put(UID, ans);
		
		if (usersAnswered(currentTurn, currentQuestion)) {
			int a = attackPlayerByTurn.get(currentTurn);
			int d = deffencePlayerByTurn.get(currentTurn);
			Question q = questionsByTurn.get(currentTurn).get(currentQuestion);
			if (answers.get(a) != q.getCorrect()) {
				winnersByTurn.put(currentTurn, d);
			} 
			else if (answers.get(d) != q.getCorrect()){
				winnersByTurn.put(currentTurn, a);
			} 			
		}	
	}
	
	public String[] getUsersAnswers(int turnNumber, int qNumber) throws IntellectException {
		String[] result = new String[playersNumber];
		for (int uid = 1; uid <= playersNumber ; uid++ ) {
			Map<Integer, Integer> usersAnswers = answersByTurn.get(turnNumber).get(qNumber);
			Integer iAnswer = usersAnswers.get(uid);
			String answer = QuestionsConstructor.answerFromInt(iAnswer);	
			result[uid - 1] = answer;
		}
		return result;
	}
	
	public Integer getWinner(int turn) {		
		return winnersByTurn.get(turn);
	}
	
	
	public void nextTurn() {
		currentTurn++;
		currentQuestion = 0;
		questionsByTurn.put(currentTurn, new HashMap<Integer, Question>());
		answersByTurn.put(currentTurn, new HashMap<Integer,Map<Integer,Integer>>());
	}
	public void beginBattle(int attackUID, int defenceUID) {
		attackPlayerByTurn.put(currentTurn, attackUID);
		deffencePlayerByTurn.put(currentTurn, defenceUID);
	}
	public void nextQuestion() throws Exception {
		currentQuestion++;
		Map<Integer,Question> turnQuestions = questionsByTurn.get(currentTurn);
		turnQuestions.put(currentQuestion, questionDAO.getRandomQuestion());
		Map<Integer, Map<Integer, Integer>> turnAnswers = answersByTurn.get(currentTurn);
		turnAnswers.put(currentQuestion, new HashMap<Integer, Integer>());
	}
	
	
	
	public void gameOver() {
		// TODO Auto-generated method stub
		
	}
	public int getCurrentQuestionNumber() {
		return currentQuestion;
	}
	

	
	public boolean usersAnswered(int turnNumber, int qNumber) {
		Map<Integer, Integer> answers = answersByTurn.get(turnNumber).get(qNumber);
		int a = attackPlayerByTurn.get(turnNumber);
		int d = deffencePlayerByTurn.get(turnNumber);
		return answers.get(a) != null && answers.get(d) != null;
	}
	
	public void forceEndAnswers() {
		Map<Integer, Integer> answers = answersByTurn.get(currentTurn).get(currentQuestion);
		int a = attackPlayerByTurn.get(currentTurn);
		int d = deffencePlayerByTurn.get(currentTurn);
		Question q = questionsByTurn.get(currentTurn).get(currentQuestion);
		
		if (answers.get(a) == null || answers.get(a) != q.getCorrect()) {
			winnersByTurn.put(currentTurn, d);
		} 
		else {
			winnersByTurn.put(currentTurn, a);
		} 			
	
	}
	
	public String getCorrectAnswer(int turnNumber, int qNumber) throws IntellectException {
		Question q = questionsByTurn.get(turnNumber).get(qNumber);
		int correct = q.getCorrect();
		return QuestionsConstructor.answerFromInt(correct);
	}
	
	public boolean isAnswerRequered(int uid) {
		int a = attackPlayerByTurn.get(currentTurn);
		int d = deffencePlayerByTurn.get(currentTurn);
		return uid == a || uid == d;
	}
	
}
