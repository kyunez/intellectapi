package ru.kyune.intellect.engine;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Set;

import ru.kyune.intellect.action.Question;
import ru.kyune.intellect.api.enums.GameState;
import ru.kyune.intellect.exc.IntellectException;

public class Game {
	
	private static final int PLAYERS_NUM = 3;
	
	GameState gameState = GameState.WAITING_FOR_PLAYERS;
	
	private HashMap<String, GamePad> gamePadByGUID = new HashMap<String, GamePad>();
	
	private GameStartManager startManager = new GameStartManager(this, PLAYERS_NUM);
	
	private MapManager mapManager;
	
	private QuestionManager qManager;
	
	private TimeManager timeManager = new TimeManager(this);
	
	public GamePad getGamePad (String userGUID) {
		GamePad pad = gamePadByGUID.get(userGUID);
		return pad;
	}
	
	public Set<String> getAllUserGUIDs() {
		return gamePadByGUID.keySet();
	}
		
	public boolean isOutOfDate() {
		return timeManager.isAllPlayersOutOfDate();
	}

	public void forceFinish() {
		// TODO Auto-generated method stub
		
	}

	public GameStartManager getStartManager() {
		return startManager;
	}
	
	synchronized void start(LinkedList<String> users) throws Exception {
		int i = 0;
		for (String GUID: users) {
			GamePad pad = this.new GamePad(GUID, ++i);		
			gamePadByGUID.put(GUID, pad);	
		}
		initialize();
		setGameState(GameState.TURN);
	} 
	
	private void initialize() {
		mapManager = new MapManager();
		qManager = new QuestionManager();
		timeManager = new TimeManager(this);
	}

	private void setGameState(GameState state) throws Exception{
		switch (state) {
			case TURN:
				timeManager.nextTurn();
				mapManager.nextTurn();
				qManager.nextTurn();
				break;
			case QUESTION:
				timeManager.nextQuestion();
				qManager.nextQuestion();	
				break;
			case GAME_OVER:
				mapManager.gameOver();
				qManager.gameOver();
			default:
				break;		
		}		
		gameState = state;
	}
	
	private void updateGameState() throws Exception {
		switch (gameState) {
		case TURN:
			int timeLeft = timeManager.getTimeLeftForTurn();
			if (timeLeft == 0) {
				mapManager.userLoosesTurn();
				setGameState(GameState.TURN);
			}
			break;
		case QUESTION:
			timeLeft = timeManager.getTimeLeftForAnswer();
			if (timeLeft == 0) {
				qManager.forceEndAnswers();
				endBattle();
			}
			break;
		default:
			break;
		}
		
	}
	
	private void endBattle() throws Exception {
		int turn = mapManager.getCurrentTurnNumber();
		Integer winner = qManager.getWinner(turn);
		if (winner == null) {
			/* it is all ready in question state but we need
			to refresh some data in setter */
			setGameState(GameState.QUESTION);
		} else {
			mapManager.conquerField();
			if (mapManager.isGameOver()) {
				setGameState(GameState.GAME_OVER);
			} else {
				setGameState(GameState.TURN);
			}
		}
	}

	public class GamePad {
		
		private int UID;
		private String GUID;
		
		GamePad (String GUID, int UID) {
			this.GUID = GUID;
			this.UID = UID;
		}
		
		public int getUID() {
			return UID;
		}

		public String getGUID() {
			return GUID;
		}

		public GameState getGameState() {
			return Game.this.gameState;			
		}

		public int getWhosTurn(int turnNumber) throws IntellectException {			
			return mapManager.getWhosTurn(turnNumber);
		}

		public void makeTurn(int x, int y) throws Exception {
			boolean isAttackMove = mapManager.makeTurn(x, y, UID);		
			if (isAttackMove) {
				int defenceUID = mapManager.getPointOwner(x, y);
				qManager.beginBattle(UID, defenceUID);
				setGameState(GameState.QUESTION);
			} else {
				/* it is all ready in turn state but we need
				to refresh some data in setter */
				setGameState(GameState.TURN);
			}
		}

		public Point getTurn(int turnNumber) throws Exception {			
			return mapManager.getTurnPoint(turnNumber);
		}

		public Question getQuestion() throws Exception {	
			return qManager.getCurrentQuestion(); 
		}

		public boolean isAnswerRequired() {
			return qManager.isAnswerRequered(UID);
		}

		public void makeAnswer(String answer) throws Exception {
			qManager.makeAnswer(answer, UID);	
			int turn = mapManager.getCurrentTurnNumber();
			int q = qManager.getCurrentQuestionNumber();
			if (qManager.usersAnswered(turn, q)) {
				endBattle();
			}
		}

		public String[] getUsersAnswers(int turnNumber, int qNumber) throws IntellectException {
			return qManager.getUsersAnswers(turnNumber, qNumber);
		}

		public String getMatchWinner(int turnNumber) throws Exception {	
			Integer winner = qManager.getWinner(turnNumber);
			String sWin;
			if (winner == null) {
				sWin = "retry";
			} else {
				sWin = winner.toString();
			}
			return sWin;
		}

		public Boolean isOutOfDate() {
			return timeManager.isUserOutOfDate(UID);
		}

		public void markTime() throws Exception {
			timeManager.markUserEnter(UID);
			updateGameState();
		}

		public int getTimeLeftForTurn() {
			int timeLeft = timeManager.getTimeLeftForTurn();
			return timeLeft;
		}

		public int getGameWinner() {
			return mapManager.getGameWinner();
		}

		public String getMap() throws Exception {
			return mapManager.getStringMap();
		}

		public int getCurrentTurnNumber() {
			return mapManager.getCurrentTurnNumber();
		}

		public int getTimeLeftForAnswer() {
			return timeManager.getTimeLeftForAnswer();
		}

		public int getCurrentQuestionNumber() {
			return qManager.getCurrentQuestionNumber();
		}

		public boolean hasUsersAnswered(int turnNumber, int qNumber) {
			return qManager.usersAnswered(turnNumber, qNumber);
		}

		public boolean isMyTurn() throws IntellectException {
			int turnNum = this.getCurrentTurnNumber();
			int whosTurn = this.getWhosTurn(turnNum);
			return whosTurn == UID;
		}

		public boolean canAnswer() {
			int attack = qManager.getAttackPlayer();
			int deffence = qManager.getDeffencePlayer();
			return (UID == attack || UID == deffence);
		}

		public String getCorrectAnswer(int turnNumber, int qNumber) throws IntellectException {
			return qManager.getCorrectAnswer(turnNumber, qNumber);
		}
		
	}
	
}
