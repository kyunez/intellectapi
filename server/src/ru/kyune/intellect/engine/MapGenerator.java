package ru.kyune.intellect.engine;

import java.util.HashMap;
import java.util.Random;
import ru.kyune.intellect.engine.Point;

public class MapGenerator {

	/**
	 * @param mapSize - input
	 * @param playersNumber - input
	 * @param playersPositions - stores results
	 * @return map array
	 */
	public static int[][] generateMap(int mapSize, int playersNumber,
			final HashMap<Integer,Point> playersPositions) {
		int mapNum = Math.abs(random.nextInt()) % const_maps.length;		
		int [][]map = new int[mapSize][mapSize];
		for (int i = 0; i < mapSize; i++) {
			for (int j = 0; j < mapSize; j++) {	
				map[i][j] = const_maps[mapNum][i][j];
			}
		}		
		for (int p = 1; p <= playersNumber; p++) {
			int point = Math.abs(random.nextInt()) % (mapSize * mapSize / playersNumber );
			int count = 0;
			end:
			for (int i = 0; i < mapSize; i++) {
				for (int j = 0; j < mapSize; j++) {
					if (map[i][j] == p) {
						if (point == count){
							map[i][j] = p + playersNumber;
							playersPositions.put(p, new Point(i + 1, j + 1));							
							break end;
						} else {
							count++;
						}
					}
				}
			}
		}
		return map;
	}
	
	/**
	 * @param mapSize - input
	 * @param playersNumber - input
	 * @param playersPositions - stores results
	 * @return map array
	 */
	public static int[][] generateEmptyMap(int mapSize, int playersNumber,
			final HashMap<Integer,Point> playersPositions) {
		int [][]map = generateMap(mapSize, playersNumber, playersPositions);
		for (int i = 0; i < mapSize; i++) {
			for (int j = 0; j < mapSize; j++) {	
				if (map[i][j] <= playersNumber) {
					map[i][j] = 0;
				}
			}
		}		
		return map;
	}
	
	private static final Random random = new Random(); 
	private static final int[][][] const_maps = { // [7][][]
		{	
			{1,1,1,3},
			{1,2,3,3},
			{1,2,2,3},
			{1,2,2,3}
		},
		{	
			{2,2,3,3},
			{2,2,3,3},
			{2,1,3,1},
			{1,1,1,1}
		},		
		{	
			{3,3,3,3},
			{1,3,1,3},
			{1,1,1,2},
			{2,2,2,2}
		},
		{	
			{3,3,3,3},
			{1,3,3,2},
			{1,1,2,2},
			{1,1,2,2}
		},
		{	
			{2,2,2,3},
			{2,2,3,3},
			{3,3,3,1},
			{1,1,1,1}
		},
		{	
			{1,2,2,2},
			{1,2,2,2},
			{1,1,3,3},
			{1,3,3,3}
		},
		{	
			{2,1,1,1},
			{2,1,1,3},
			{2,3,3,3},
			{2,2,3,3}
		}		
	};
	
}
