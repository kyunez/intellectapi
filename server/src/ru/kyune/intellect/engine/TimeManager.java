package ru.kyune.intellect.engine;

import java.util.HashMap;

import ru.kyune.intellect.common.PropertyManager;

public class TimeManager {
	private static final long playersNumber = PropertyManager.getPlayersNumber();
	
	private static final long TURN_TIME = PropertyManager.getTimeForTurn();
	private static final long ANSWER_TIME = PropertyManager.getTimeForAnswer();
	private static final long NETWORK_DELAY = PropertyManager.getNetworkDelay();
	private static final long CLIENT_REQUEST_DELAY = PropertyManager.getClientRequestDelay();
	
	/**
	 *  uid -> time
	 */
	private HashMap<Integer, Long> usersEnters = new HashMap<Integer, Long>();
	private long lastTurnStarted;
	private long lastQuestionStarted;
	
	private Game game;
	
	public TimeManager(Game game) {
		this.game = game;
	}
	
	public void markUserEnter(int uid) {
		usersEnters.put(uid, System.currentTimeMillis());
	}

	public Boolean isUserOutOfDate(int uid) {
		Long mark = usersEnters.get(uid);
		if (mark == null) {
			return false;
		}
		switch (game.gameState) {
		case QUESTION:
			if (mark > lastQuestionStarted) {
				return false;
			} else if (System.currentTimeMillis() - mark 
					> CLIENT_REQUEST_DELAY + NETWORK_DELAY) {
				return true;
			} else {
				return false;
			}
		case TURN:
			if (mark > lastTurnStarted) {
				return false;
			} else if (System.currentTimeMillis() - mark 
					> CLIENT_REQUEST_DELAY + NETWORK_DELAY) {
				return true;
			} else {
				return false;
			}
		default:
			if (System.currentTimeMillis() - mark 
					> CLIENT_REQUEST_DELAY + NETWORK_DELAY) {
				return true;
			} else {
				return false;
			}
		}
	}

	public void nextTurn() {
		lastTurnStarted = System.currentTimeMillis();
	}

	public boolean isAllPlayersOutOfDate() {
		boolean result = true;
		for (int uid = 1; uid <= playersNumber; uid++){
			result &= isUserOutOfDate(uid);
		}
		return result;
	}

	public int getTimeLeftForTurn() {
		Long millis = TURN_TIME - (System.currentTimeMillis() - lastTurnStarted) 
				+ CLIENT_REQUEST_DELAY + NETWORK_DELAY;
		millis = millis > TURN_TIME ? TURN_TIME : millis;
		millis = millis < 0 ? 0 : millis;
		return  (int) (millis/1000);
	}

	public void nextQuestion() {
		lastQuestionStarted = System.currentTimeMillis();
	}

	public int getTimeLeftForAnswer() {
		Long millis = ANSWER_TIME - (System.currentTimeMillis() - lastTurnStarted) 
				+ CLIENT_REQUEST_DELAY + NETWORK_DELAY;
		millis = millis > ANSWER_TIME ? ANSWER_TIME : millis;
		millis = millis < 0 ? 0 : millis;
		return  (int) (millis/1000);
	}

}
