package ru.kyune.intellect.engine;

import java.util.HashMap;

import ru.kyune.intellect.common.PropertyManager;
import ru.kyune.intellect.exc.IntellectException;

class MapManager {
	private boolean gameOver = false;

	private static final int playersNumber = PropertyManager.getPlayersNumber();;
	private static final int mapSize = PropertyManager.getMapSize();

	private int [][] map;
	
	private int currentTurn = 0;
	
	private HashMap<Integer, Point> targetsByTurn = new HashMap<Integer, Point>();
	
	/**
	 * turn -> uid
	 */
	private HashMap<Integer, Integer> whosTurn = new HashMap<Integer, Integer>();
	
	private HashMap<Integer,Point> playersPositions = new HashMap<Integer, Point>();
	
	public MapManager() {
		map = MapGenerator.generateEmptyMap(mapSize, playersNumber, playersPositions);
	}
	
	private void mapSimpleMove() throws IntellectException {
		Point position = playersPositions.get(getWhosCurrentTurn());
		Point target =  targetsByTurn.get(getCurrentTurnNumber());
		
		map[position.x - 1][position.y - 1] = getWhosCurrentTurn();
		map[target.x - 1][target.y - 1] = getWhosCurrentTurn() + playersNumber;
		
		playersPositions.put(getWhosCurrentTurn(), target);
	}
	
	private boolean isCorrectPointToMove(Point position, Point turnPoint) {
		int x = turnPoint.x;
		int y = turnPoint.y;
		int diff = Math.abs(position.x - x) + Math.abs(position.y - y);
		if (diff > 1 || x < 1 || y < 1 || x > mapSize || y > mapSize) {
			return false;
		}
		return true;
	}
	
	public String getStringMap() throws Exception {
		StringBuilder sb = new StringBuilder("");
		for (int i = 0; i < mapSize; i++) {
			for (int j = 0; j < mapSize; j++) {
				sb.append(map[i][j]);
			}
		}									
		return sb.toString();
	}

	public int getWhosTurn(int turnNumber) throws IntellectException {
		Integer uid = whosTurn.get(turnNumber);
		if (uid == null) {
			throw new IntellectException("Error. current turn number " + getCurrentTurnNumber() 
					+ " is less than Requested: " + turnNumber); 
		}
		return uid;
	}
	
	/**
	 * @return if this turn is attacking another player returns - true;
	 * otherwise - false 
	 * @throws Exception
	 */
	public boolean makeTurn(int x, int y, int UID) throws Exception {
		Point position = playersPositions.get(UID);
		Point target = new Point(x, y);			
		if (!isCorrectPointToMove(position, target)){
			throw new Exception("incorrect point to Turn");
		}		
		
		targetsByTurn.put(getCurrentTurnNumber(), target);
		
		int targetUID = map[x - 1][y - 1];								
		if (targetUID == getWhosCurrentTurn()
				|| targetUID == getWhosCurrentTurn() + playersNumber
				|| targetUID == 0) {
			mapSimpleMove();
			return false;
		} else {
			return true;
		}
	}

	public Point getTurnPoint(int turnNumber) {
		return targetsByTurn.get(turnNumber);
	}

	public void conquerField() throws IntellectException {
		Point positionP = playersPositions.get(getWhosCurrentTurn());
		Point targetP =  targetsByTurn.get(getCurrentTurnNumber());
		
		int target = map[targetP.x - 1][targetP.y - 1];
		int uid = getWhosCurrentTurn();		
		if (target <= playersNumber) {
			map[positionP.x - 1][positionP.y - 1] = uid;
			map[targetP.x - 1][targetP.y - 1] = uid + playersNumber;
		} else {
			int killedUID = target - playersNumber;
			for (int i = 0; i < playersNumber; i++) {
				for (int j = 0; j < playersNumber; j++) {
					if (map[i][j] == killedUID) {
						map[i][j] = uid;
					}
				}
			}
			gameOver();
		}
		playersPositions.put(uid, targetP);
	}

	public void nextTurn() {
		currentTurn++;
		int uid = (currentTurn % playersNumber) + 1;
		whosTurn.put(currentTurn, uid);
	}

	public void userLoosesTurn() throws Exception {
		int uid = getWhosCurrentTurn();
		Point p = playersPositions.get(uid);
		makeTurn(p.x, p.y, uid);
	}

	public int getCurrentTurnNumber() {
		return currentTurn;
	}

	public void gameOver() {
		gameOver = true;
	}

	public int getWhosCurrentTurn() throws IntellectException {
		return getWhosTurn(getCurrentTurnNumber());
	}

	public int getPointOwner(int x, int y) {
		int uid = map[x - 1][y - 1];
		if (uid > playersNumber) {
			uid -= playersNumber;
		}
		return uid;
	}

	public boolean isGameOver() {
		return gameOver;
	}

	public int getGameWinner() {
		int [] score = new int[playersNumber];
		for (int uid = 0; uid < playersNumber; uid++) {
			score[uid] = 0;
			for (int i = 0; i < playersNumber; i++) {
				for (int j = 0; j < playersNumber; j++) {
					if (map[i][j] == uid + 1) {
						score[uid]++;
					}
				}
			}
		}
		if (score[0] > score[1] && score[0] > score[2]) {
			return 1;
		} else if (score[1] > score[2]) {
			return 2;
		}
		return 3;
	}
	
}
