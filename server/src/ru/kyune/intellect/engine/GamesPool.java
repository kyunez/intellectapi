package ru.kyune.intellect.engine;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

public class GamesPool {
	
	private Game fillInGame = new Game();
	
	private ConcurrentHashMap<String, Game> activeGamesByUserGUID = new ConcurrentHashMap<String, Game>();
	private ConcurrentHashMap<String, Game> waitningGamesByUserGUID = new ConcurrentHashMap<String, Game>();
			
	public GamesPool() {
		
	}
	
	public synchronized Game enterNewGame(String userGUID) throws Exception {	
		Game game = activeGamesByUserGUID.get(userGUID);
		if (game != null) {
			removeGameMapping(userGUID);
		}
		
		game = waitningGamesByUserGUID.get(userGUID);
		if (game != null) {
			GameStartManager gsm = game.getStartManager();
			if (gsm.isGameReadyToStart()) { 
				waitningGamesByUserGUID.remove(userGUID);
				activeGamesByUserGUID.put(userGUID, game);
				return game;
			} else {
				return null;
			}
		} else { 
			Game buffer = fillInGame;
			
			GameStartManager gsm = fillInGame.getStartManager();
			
			gsm.registerUser(userGUID);
			
			if (gsm.isGameReadyToStart()) {		
				activeGamesByUserGUID.put(userGUID, fillInGame);	
				gsm.startGame();
				fillInGame = new Game();	
				return buffer;
			} else {
				waitningGamesByUserGUID.put(userGUID, fillInGame);
				return null;
			}	
		}
	}
	
	public Game getGameByUserGUID(String GUID) {
		return activeGamesByUserGUID.get(GUID);		
	}

	public Collection<Game> getAllGames() {
		HashSet<Game> games = new HashSet<Game>();
		for (Entry<String,Game> gameEntry: activeGamesByUserGUID.entrySet()) {
			games.add(gameEntry.getValue());
		}
		return games;
	}

	public void removeGameMapping(String userGUID) {
			activeGamesByUserGUID.remove(userGUID);
	}
	
}
