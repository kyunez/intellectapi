package ru.kyune.intellect.engine;

import java.util.LinkedList;

import ru.kyune.intellect.api.enums.GameState;

public class GameStartManager {
	
	private int playersNumber;
	private Game game;
	private int playersEntered = 0;
	private LinkedList<String> usersEntered = new LinkedList<String>();
	
	public  GameStartManager(Game game, int playersNumber) {
		this.game = game;
		this.playersNumber = playersNumber;
	}
	
	public void registerUser(String userGUID) throws Exception {
		if (game.gameState != GameState.WAITING_FOR_PLAYERS) {
			throw new Exception("Can't enter the game - it's full");
		}
		playersEntered++;
		usersEntered.add(userGUID);
	}
	
	public boolean isGameReadyToStart() {
		return playersEntered == playersNumber;
	}
	
	public void startGame() throws Exception {
		if (playersEntered == playersNumber) {
			game.start(usersEntered);
		} else {
			throw new Exception("Not enough users to start game"); 
		}
	}
	

}
