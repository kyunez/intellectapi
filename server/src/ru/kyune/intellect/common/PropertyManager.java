package ru.kyune.intellect.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.ServletContext;

public class PropertyManager {
	static private Properties globalProperies; 
	
	public static void init(ServletContext sc) throws IOException {
		InputStream stream = sc.getResourceAsStream("/WEB-INF/cfg/global.properties");
		globalProperies = new Properties(); 
		globalProperies.load(stream);
	}
	
	public static int getGarbageDaemonDelay() {
		String gddelay = globalProperies.getProperty("gddelay");
		Integer delay = Integer.valueOf(gddelay); 
		return delay;
	}

	public static int getPlayersNumber() {
		String playersNumber = globalProperies.getProperty("playersNumber");
		Integer num = Integer.valueOf(playersNumber); 
		return num;
	}

	public static int getMapSize() {
		String mapSize = globalProperies.getProperty("mapSize");
		Integer size = Integer.valueOf(mapSize); 
		return size;
	}

	public static long getTimeForTurn() {
		String time = globalProperies.getProperty("turnTime");
		Long t = Long.valueOf(time); 
		return t;
	}

	public static long getTimeForAnswer() {
		String time = globalProperies.getProperty("questionTime");
		Long t = Long.valueOf(time); 
		return t;
	}

	public static long getNetworkDelay() {
		String time = globalProperies.getProperty("networkDelay");
		Long t = Long.valueOf(time); 
		return t;
	}

	public static long getClientRequestDelay() {
		String time = globalProperies.getProperty("requestDelay");
		Long t = Long.valueOf(time); 
		return t;
	}

	public static String getDBurl() {
		return globalProperies.getProperty("DB_url");
	}

	public static String getDBuser() {
		return globalProperies.getProperty("DB_userName");
	}

	public static String getDBpassword() {
		return globalProperies.getProperty("DB_userPassword");
	}

	public static String getDBname() {
		return globalProperies.getProperty("DB_name");
	}

	
}
