package ru.kyune.intellect.action;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import ru.kyune.intellect.api.enums.GameState;
import ru.kyune.intellect.api.enums.ResponseStatus;
import ru.kyune.intellect.api.json.request.GUIDRequest;
import ru.kyune.intellect.api.json.response.GetGameResultResponse;
import ru.kyune.intellect.api.json.response.ResponseGeneric;
import ru.kyune.intellect.engine.Game;
import ru.kyune.intellect.engine.Game.GamePad;

@WebServlet("/getGameResult")
public class GetGameResultServlet  extends AbstractServlet {
	private static final long serialVersionUID = 6599996789041298129L;
			
	@Override
	protected ResponseGeneric execute(HttpServletRequest request)
			throws IOException, Exception {
		GetGameResultResponse resp = new GetGameResultResponse();
		
		GUIDRequest jsonRequest = gsonParser.fromJson(getRequestBody(request), GUIDRequest.class);
		Game game = pool.getGameByUserGUID(jsonRequest.GUID);
		synchronized (game) {
			GamePad pad = game.getGamePad(jsonRequest.GUID);
			
			GameState gameState = pad.getGameState();
			switch (gameState) {
				case GAME_OVER:
					resp.status = ResponseStatus.OK.getStatusName();
					resp.winner = pad.getGameWinner();
					return resp;		
				default:
					resp.status = ResponseStatus.INVALID.getStatusName();
					return resp;
			}	
		}
	}
  
}

