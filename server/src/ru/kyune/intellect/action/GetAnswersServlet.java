package ru.kyune.intellect.action;
import java.io.*;

import javax.servlet.annotation.*;
import javax.servlet.http.*;

import ru.kyune.intellect.api.enums.GameState;
import ru.kyune.intellect.api.enums.ResponseStatus;
import ru.kyune.intellect.api.json.request.GetAnswersRequest;
import ru.kyune.intellect.api.json.response.GetAnswersResponse;
import ru.kyune.intellect.api.json.response.ResponseGeneric;
import ru.kyune.intellect.engine.Game;
import ru.kyune.intellect.engine.Game.GamePad;


@WebServlet("/getAnswers")
public class GetAnswersServlet  extends AbstractServlet {
	private static final long serialVersionUID = -5193675325384018614L;
			
	@Override
	protected ResponseGeneric execute(HttpServletRequest request)
			throws IOException, Exception {
		GetAnswersResponse resp = new GetAnswersResponse();
		
		GetAnswersRequest jsonRequest = gsonParser.fromJson(getRequestBody(request), GetAnswersRequest.class);
		Game game = pool.getGameByUserGUID(jsonRequest.GUID);
		synchronized (game) {
			GamePad pad = game.getGamePad(jsonRequest.GUID);
			Boolean isOutOfDate = checkIfUserSuspendedAndMarkTime(pad);
			if (isOutOfDate) {
				resp.status = ResponseStatus.OUT_OF_DATE.getStatusName();
				return resp;
			}
			
			GameState gameState = pad.getGameState();
			switch (gameState) {
				case TURN:
				case QUESTION:
				case GAME_OVER:
					boolean qIsAnswered = pad.hasUsersAnswered(jsonRequest.turnNumber, 
							jsonRequest.qNumber);
					if (qIsAnswered) {
						resp.status = ResponseStatus.OK.getStatusName();
						String[] answers = pad.getUsersAnswers(jsonRequest.turnNumber, 
								jsonRequest.qNumber);				
						resp.user1 = answers[0];
						resp.user2 = answers[1];
						resp.user3 = answers[2];
						resp.correct = pad.getCorrectAnswer(jsonRequest.turnNumber, 
								jsonRequest.qNumber);
						resp.winner = pad.getMatchWinner(jsonRequest.turnNumber);							
						return resp;
					} else {
						resp.status = ResponseStatus.RETRY.getStatusName();
						return resp;	
					}
				default:
					resp.status = ResponseStatus.INVALID.getStatusName();
					return resp;
			}	
		}
	}
	
}


