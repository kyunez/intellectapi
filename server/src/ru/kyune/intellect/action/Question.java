package ru.kyune.intellect.action;

public class Question {
	
	private String text;
	private String A;
	private String B;
	private String C;
	private String D;
	private int correct;
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getA() {
		return A;
	}
	public void setA(String a) {
		A = a;
	}
	public String getB() {
		return B;
	}
	public void setB(String b) {
		B = b;
	}
	public String getC() {
		return C;
	}
	public void setC(String c) {
		C = c;
	}
	public String getD() {
		return D;
	}
	public void setD(String d) {
		D = d;
	}
	public int getCorrect() {
		return correct;
	}
	public void setCorrect(int correct) {
		this.correct = correct;
	}
	
}
