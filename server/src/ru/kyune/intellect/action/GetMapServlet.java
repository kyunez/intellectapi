package ru.kyune.intellect.action;
import java.io.*;

import javax.servlet.annotation.*;
import javax.servlet.http.*;

import ru.kyune.intellect.api.enums.GameState;
import ru.kyune.intellect.api.enums.ResponseStatus;
import ru.kyune.intellect.api.json.request.GUIDRequest;
import ru.kyune.intellect.api.json.response.GetMapResponse;
import ru.kyune.intellect.api.json.response.ResponseGeneric;
import ru.kyune.intellect.engine.Game;
import ru.kyune.intellect.engine.Game.GamePad;


@WebServlet("/getMap")
public class GetMapServlet  extends AbstractServlet {
	private static final long serialVersionUID = 7713810230606466456L;
			
	@Override
	protected ResponseGeneric execute(HttpServletRequest request) 
			throws IOException, Exception {
		GetMapResponse resp = new GetMapResponse();	
		
		GUIDRequest jsonRequest = gsonParser.fromJson(getRequestBody(request), GUIDRequest.class);
		Game game = pool.getGameByUserGUID(jsonRequest.GUID);
		synchronized (game) {
			GamePad pad = game.getGamePad(jsonRequest.GUID);
			Boolean isOutOfDate = checkIfUserSuspendedAndMarkTime(pad);
			if (isOutOfDate) {
				resp.status = ResponseStatus.OUT_OF_DATE.getStatusName();
				return resp;
			}
			
			GameState gameState = pad.getGameState();
			switch (gameState) {
				case GAME_OVER:
					resp.status = ResponseStatus.GAME_OVER.getStatusName();
					resp.map = pad.getMap();				
					return resp;
				case WAITING_FOR_PLAYERS:
					resp.status = ResponseStatus.INVALID.getStatusName();			
					return resp;
				case TURN:
					resp.status = ResponseStatus.OK.getStatusName();
					resp.map =  pad.getMap();	
					resp.turnNumber = pad.getCurrentTurnNumber();
					resp.whosTurn = pad.getWhosTurn(resp.turnNumber);
					resp.timeLeft = pad.getTimeLeftForTurn();
					return resp;
				default:
					resp.status = ResponseStatus.OK.getStatusName();
					resp.map =  pad.getMap();	
					return resp;
			}	
		}
	}
  
}


