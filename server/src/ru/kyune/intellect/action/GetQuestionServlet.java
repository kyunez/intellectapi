package ru.kyune.intellect.action;
import java.io.*;

import javax.servlet.annotation.*;
import javax.servlet.http.*;

import ru.kyune.intellect.api.enums.GameState;
import ru.kyune.intellect.api.enums.ResponseStatus;
import ru.kyune.intellect.api.json.request.GUIDRequest;
import ru.kyune.intellect.api.json.response.GetQuestionResponse;
import ru.kyune.intellect.api.json.response.ResponseGeneric;
import ru.kyune.intellect.engine.Game;
import ru.kyune.intellect.engine.Game.GamePad;


@WebServlet("/getQuestion")
public class GetQuestionServlet  extends AbstractServlet {
	private static final long serialVersionUID = -5193675325384018614L;
			
	@Override
	protected ResponseGeneric execute(HttpServletRequest request) 
			throws IOException, Exception {
		
		GetQuestionResponse resp = new GetQuestionResponse();
		
		GUIDRequest jsonRequest = gsonParser.fromJson(getRequestBody(request), GUIDRequest.class);
		Game game = pool.getGameByUserGUID(jsonRequest.GUID);
		
		synchronized (game) {
			GamePad pad = game.getGamePad(jsonRequest.GUID);
			Boolean isOutOfDate = checkIfUserSuspendedAndMarkTime(pad);
			if (isOutOfDate) {
				resp.status = ResponseStatus.OUT_OF_DATE.getStatusName();
				return resp;
			}
			
			GameState gameState = pad.getGameState();
			switch (gameState) {
				case QUESTION:
					resp.status = ResponseStatus.OK.getStatusName();
					Question q = pad.getQuestion();
					resp.text = q.getText();
					resp.A = q.getA();
					resp.B = q.getB();
					resp.C = q.getC();
					resp.D = q.getD();
					resp.timeLeft = pad.getTimeLeftForAnswer();
					boolean needAnswer = pad.isAnswerRequired();
					if (needAnswer){
						resp.needAnswer = "true";
					} else {
						resp.needAnswer = "false";
					}
					resp.turnNumber = pad.getCurrentTurnNumber();
					resp.qNumber = pad.getCurrentQuestionNumber();
					return resp;		
				default:
					resp.status = ResponseStatus.INVALID.getStatusName();
					return resp;
			}		
		}
	}	
	  
}


