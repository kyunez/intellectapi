package ru.kyune.intellect.action;
import java.io.*;

import javax.servlet.annotation.*;
import javax.servlet.http.*;

import ru.kyune.intellect.api.enums.GameState;
import ru.kyune.intellect.api.enums.ResponseStatus;
import ru.kyune.intellect.api.json.request.MakeTurnRequest;
import ru.kyune.intellect.api.json.response.ResponseGeneric;
import ru.kyune.intellect.engine.Game;
import ru.kyune.intellect.engine.Game.GamePad;

@WebServlet("/makeTurn")
public class MakeTurnServlet  extends AbstractServlet {
	private static final long serialVersionUID = 6102301608044011289L;
			
	@Override
	protected ResponseGeneric execute(HttpServletRequest request)
			throws IOException, Exception {
		ResponseGeneric resp = new ResponseGeneric();
		
		MakeTurnRequest jsonRequest = gsonParser.fromJson(getRequestBody(request), MakeTurnRequest.class);
		Game game = pool.getGameByUserGUID(jsonRequest.GUID);
		synchronized (game) {
			GamePad pad = game.getGamePad(jsonRequest.GUID);
			Boolean isOutOfDate = checkIfUserSuspendedAndMarkTime(pad);
			if (isOutOfDate) {
				resp.status = ResponseStatus.OUT_OF_DATE.getStatusName();
				return resp;
			}
			
			GameState gameState = pad.getGameState();
			switch (gameState) {
				case TURN:
					if (!pad.isMyTurn()) {
						resp.status = ResponseStatus.INVALID.getStatusName();
						return resp;
					}				
					pad.makeTurn(jsonRequest.x, jsonRequest.y);				
					resp.status = ResponseStatus.OK.getStatusName();
					return resp;
				default:
					resp.status = ResponseStatus.INVALID.getStatusName();
					return resp;
			}		
		}
	}
  
}


