package ru.kyune.intellect.action;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import ru.kyune.intellect.api.enums.GameState;
import ru.kyune.intellect.api.enums.ResponseStatus;
import ru.kyune.intellect.api.json.request.GUIDRequest;
import ru.kyune.intellect.api.json.response.GetGameStateResponse;
import ru.kyune.intellect.api.json.response.ResponseGeneric;
import ru.kyune.intellect.engine.Game;
import ru.kyune.intellect.engine.Game.GamePad;

@WebServlet("/getGameState")
public class GetGameStateServlet  extends AbstractServlet {
	private static final long serialVersionUID = -5193675325384018614L;
			
	@Override
	protected ResponseGeneric execute(HttpServletRequest request)
			throws IOException, Exception {
		GetGameStateResponse resp = new GetGameStateResponse();
		
		GUIDRequest jsonRequest = gsonParser.fromJson(getRequestBody(request), GUIDRequest.class);
		Game game = pool.getGameByUserGUID(jsonRequest.GUID);
		synchronized (game) {
			GamePad pad = game.getGamePad(jsonRequest.GUID);
			pad.markTime();
			
			GameState gameState = pad.getGameState();
			resp.status = ResponseStatus.OK.getStatusName();
			resp.state = gameState.getStateName();
			
			return resp;
		}
	}
	
}
