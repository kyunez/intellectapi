package ru.kyune.intellect.action;
import java.io.*;

import javax.servlet.annotation.*;
import javax.servlet.http.*;

import ru.kyune.intellect.api.enums.ResponseStatus;
import ru.kyune.intellect.api.json.request.GUIDRequest;
import ru.kyune.intellect.api.json.response.ResponseGeneric;
import ru.kyune.intellect.api.json.response.StartGameResponse;
import ru.kyune.intellect.engine.Game;
import ru.kyune.intellect.engine.Game.GamePad;

@WebServlet("/startGame")
public class StartGameServlet  extends AbstractServlet {
	private static final long serialVersionUID = 2789833837475351742L;
	
	@Override
	protected ResponseGeneric execute(HttpServletRequest request) 
			throws IOException, Exception {
		StartGameResponse resp = new StartGameResponse();
		
		GUIDRequest jsonRequest = gsonParser.fromJson(getRequestBody(request), GUIDRequest.class);
		String GUID = jsonRequest.GUID;
		
		Game game = pool.enterNewGame(GUID);
		
		if (game != null) {
			resp.status = ResponseStatus.OK.getStatusName();
			GamePad pad = game.getGamePad(GUID);
			int uid = pad.getUID();
			resp.UID = uid;
		} else {
			resp.status = ResponseStatus.RETRY.getStatusName();
		}	
		return resp;
	}
	
}


