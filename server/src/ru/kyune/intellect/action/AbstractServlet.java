package ru.kyune.intellect.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ru.kyune.intellect.api.json.response.ResponseGeneric;
import ru.kyune.intellect.common.ContextAttributes;
import ru.kyune.intellect.engine.GamesPool;
import ru.kyune.intellect.engine.Game.GamePad;
import ru.kyune.intellect.utils.ThreadSafeJson;

public abstract class AbstractServlet extends HttpServlet {
	
	private Logger log = Logger.getLogger(this.getClass());
	
	private static final long serialVersionUID = -224998820389189229L;
	
	protected ThreadSafeJson gsonParser = new ThreadSafeJson();	
	
	protected GamesPool pool;
	
	private void initResponse( HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");				
		response.setContentType("application/json");   
	}
	
	/*  
	 * Every child of this class must call super.doPost() in overridden doPost() method
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {	
		initResponse(response);
		
		ResponseGeneric jsonResponse = new ResponseGeneric();
		try {		
			pool = (GamesPool) getServletContext().getAttribute(
					ContextAttributes.GAMES_POOL);
			if (pool == null) {
				throw new Exception("Game pool is not initialised");
			}	
			
			jsonResponse = execute(request); //main work is here!
			
		} catch (Exception e) {
			jsonResponse.status = "error";
			log.error(e);
		} finally {			
			String s = gsonParser.toJson(jsonResponse);
			response.getWriter().write(s);
			response.getWriter().flush();
		}	
	}

	protected abstract ResponseGeneric execute(HttpServletRequest request) throws IOException, Exception;
	
	protected String getRequestBody(HttpServletRequest request)
			throws IOException {
		String requestBody;		
		StringBuilder sb = new StringBuilder();
		String s;
		while ((s = request.getReader().readLine()) != null) {
	        sb.append(s);
	    }		
		requestBody = sb.toString();
		return requestBody;
	}
	
	protected boolean checkIfUserSuspendedAndMarkTime(GamePad pad) throws Exception {
		Boolean isOutOfDate = pad.isOutOfDate();
		pad.markTime();
		return isOutOfDate;
	}
	
}
