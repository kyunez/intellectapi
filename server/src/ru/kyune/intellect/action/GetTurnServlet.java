package ru.kyune.intellect.action;
import java.io.*;

import javax.servlet.annotation.*;
import javax.servlet.http.*;

import ru.kyune.intellect.api.enums.GameState;
import ru.kyune.intellect.api.enums.ResponseStatus;
import ru.kyune.intellect.api.json.request.GetTurnRequest;
import ru.kyune.intellect.api.json.response.GetTurnResponse;
import ru.kyune.intellect.api.json.response.ResponseGeneric;
import ru.kyune.intellect.engine.Game;
import ru.kyune.intellect.engine.Game.GamePad;
import ru.kyune.intellect.engine.Point;


@WebServlet("/getTurn")
public class GetTurnServlet  extends AbstractServlet {
	private static final long serialVersionUID = 5201847697161272029L;

			
	@Override
	protected ResponseGeneric execute(HttpServletRequest request) throws IOException, Exception {
		GetTurnResponse resp = new GetTurnResponse();	
		
		GetTurnRequest jsonRequest = gsonParser.fromJson(getRequestBody(request), GetTurnRequest.class);
		Game game = pool.getGameByUserGUID(jsonRequest.GUID);
		synchronized (game) {
			GamePad pad = game.getGamePad(jsonRequest.GUID);
			Boolean isOutOfDate = checkIfUserSuspendedAndMarkTime(pad);
			if (isOutOfDate) {
				resp.status = ResponseStatus.OUT_OF_DATE.getStatusName();
				return resp;
			}
			
			GameState gameState = pad.getGameState();
			switch (gameState) { 
				case TURN:
				case QUESTION:
				case GAME_OVER:
					Point p = pad.getTurn(jsonRequest.turnNumber);
					if (p == null) {
						resp.status = ResponseStatus.RETRY.getStatusName();
						return resp;
					} else {
						resp.status = ResponseStatus.OK.getStatusName();
						resp.uid =  pad.getWhosTurn(jsonRequest.turnNumber);
						resp.x = p.getX();
						resp.y = p.getY();
						return resp;
					}
				default:
					resp.status = ResponseStatus.INVALID.getStatusName();
					return resp;
			}	
		}
	}

}


