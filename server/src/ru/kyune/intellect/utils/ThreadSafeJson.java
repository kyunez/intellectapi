package ru.kyune.intellect.utils;

import ru.kyune.intellect.api.json.response.ResponseGeneric;

import com.google.gson.Gson;

public class ThreadSafeJson {
	private Gson parser = new Gson();

	synchronized public String toJson(ResponseGeneric jsonResponse) {
		return parser.toJson(jsonResponse);
	}

	synchronized public <T> T fromJson(String requestBody, Class<T> c) {
		return parser.fromJson(requestBody, c );
	}

}
