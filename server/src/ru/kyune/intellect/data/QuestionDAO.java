package ru.kyune.intellect.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;

import ru.kyune.intellect.action.Question;

public class QuestionDAO {
	
	private static Random random = new Random(); 
	
	private Integer minID;
	private Integer maxID;	

	public Question getRandomQuestion() throws Exception {
		Question q = new Question();
		Connection con = DBConnectionFactory.getDBconnection();
		
		if (minID == null || maxID == null) {			
			initRange(con);			
		}
		
		try { 
			PreparedStatement randomStmt = con.prepareStatement("SELECT * FROM `intellect`.`questions` WHERE `id` >= ? LIMIT 0,1");
			
			int randomInt = Math.abs(random.nextInt()) % (maxID - minID);
			randomStmt.setInt(1, randomInt + minID);
			
			ResultSet randomSet = randomStmt.executeQuery();
			if (randomSet.next()) {
				String text = randomSet.getString(2);
				String variants = randomSet.getString(3);
				int answer = randomSet.getInt(4);
				
				q.setText(text);
				q.setCorrect(answer);			
				String vars[] = variants.split("#@");
				q.setA(vars[0]);
				q.setB(vars[1]);
				q.setC(vars[2]);
				q.setD(vars[3]);			
			} 
			else {
				throw new Exception ("DB returned no question rows.");
			}
					
			
			return q;
		} finally {
			con.close();
		}
		
	}


	private void initRange(Connection con) throws SQLException {
		PreparedStatement range = con.prepareStatement("SELECT MAX(`id`) AS max_id , MIN(`id`) AS min_id FROM `intellect`.`questions`");
		ResultSet rangeSet = range.executeQuery();
		if (rangeSet.next()){
			maxID = rangeSet.getInt(1);
			minID = rangeSet.getInt(2);
		}
	}

}
