package ru.kyune.intellect.test;

import java.io.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.*;

import com.google.gson.Gson;

import ru.kyune.intellect.data.DBConnectionFactory;


@WebServlet("/testDB")
public class TestDBServlet extends HttpServlet {
	private static final long serialVersionUID = 8453220128474152121L;

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
	    
		try {
			Connection dbConnection = DBConnectionFactory.getDBconnection();
			
			java.sql.PreparedStatement st = dbConnection.prepareStatement(
					"SELECT * FROM intellect.Questions WHERE id = 13");
			
			ResultSet set = st.executeQuery();
			
			if (set.next()){
				response.setCharacterEncoding("UTF-8");				
				response.setContentType("application/json");        
				Gson gson = new Gson();
				try {	

					StringBuilder sb = new StringBuilder();
					String s;
					while ((s = request.getReader().readLine()) != null) {
		                sb.append(s);
		            }
					
					Req rq = (Req)gson.fromJson(sb.toString(), Req.class);
					
					/*Student student = (Student) gson.fromJson(sb.toString(), Student.class);
					Status status = new Status();
					if (student.getName().equalsIgnoreCase("edw")) {
						status.setSuccess(true);
						status.setDescription("success");
					} else {
						status.setSuccess(false);
			            status.setDescription("not edw");
					}*/
					
					Resp r = new Resp();
					r.newName = rq.name.toUpperCase();
					r.count = rq.x * rq.y;
					
					response.getOutputStream().print(gson.toJson(r));
					response.getOutputStream().flush();
					
				} catch (Exception ex) {
					ex.printStackTrace();
					/*Status status = new Status();
					status.setSuccess(false);
					status.setDescription(ex.getMessage());
					response.getOutputStream().print(gson.toJson(status));
					response.getOutputStream().flush();*/
				} 
			}
			
			dbConnection.close();
			
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		

	}
	
	class Req {
		
		Req(){}
		
		int x;
		int y;
		String name;
		
	}
	
	class Resp{
		Resp(){}
		
		String newName;
		int count;
	}
  
}
