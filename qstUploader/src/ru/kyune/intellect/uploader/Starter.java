package ru.kyune.intellect.uploader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class Starter {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		for (int q = 15;q<16; q++) {
		
		String fileName = "./res/qstBase1/q%d.txt";
		fileName = String.format(fileName, q);
		
		
		File f = new File(fileName);
		FileReader fin;
		
		try {
		
			fin = new FileReader(f);
			BufferedReader reader = new BufferedReader(fin);
			String sNum = reader.readLine();
			String sn = sNum.replaceAll("[^0-9]", "");
			int n = Integer.valueOf(sn);
			
			Connection conn = null;
			String url = "jdbc:mysql://localhost:3306/";
			
			Properties properties=new Properties();
			properties.setProperty("user","root");
			properties.setProperty("password","root");

			
			
			String dbName = "intellect";
			String driver = "com.mysql.jdbc.Driver";
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url+dbName,properties);
		

			conn.prepareStatement("SET NAMES 'utf8'").execute();
			
			java.sql.PreparedStatement st = conn.prepareStatement(
					"insert into intellect.Questions (text, variants, answer, difficulty) VALUES(?,?,?,?)");
			
			
			for (int i = 0; i < n; i++) {
				String s = reader.readLine();
				String ss[] = s.split("#@");
				
				st.setString(1, ss[0]);
				String variants = ss[1];
				
				for (int j = 2; j< 5; j++) {
					variants += "#@" + ss[j];					
				}
				st.setString(2, variants);
				
				if (ss[5].trim().equals("A")){
					st.setInt(3, 1);
				} 
				else if (ss[5].trim().equals("B")){
					st.setInt(3, 2);
				} 
				else if (ss[5].trim().equals("C")){
					st.setInt(3, 3);
				} 
				else if (ss[5].trim().equals("D")){
					st.setInt(3, 4);
				} 
				
				double dif = Double.valueOf(q)/15d;
				
				st.setFloat(4, (float) dif);
				
				st.execute();

				
			} 
			
			reader.close();
			conn.close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		}
		
		

	}

}
