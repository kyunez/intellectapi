package ru.kyune.intellect.api.enums;

public enum GameState {
	TURN("turn"),
	QUESTION("question"),
	GAME_OVER("gameOver"),
	WAITING_FOR_PLAYERS("WaitingForPlayers");
	
	private String stateName;
	
	private GameState(String stateName){
		this.stateName = stateName;
	}
	
	public String getStateName() {
		return stateName;
	}
	
	public static GameState fromString(String name) {
		for (GameState state : GameState.values()) {
			if (state.getStateName().equals(name)) {
				return state;
			}
		}
		return null;
	}

}
