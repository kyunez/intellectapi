package ru.kyune.intellect.api.enums;

public enum ResponseStatus {
	OK("ok"),
	ERROR("error"),
	RETRY("retry"),
	OUT_OF_DATE("outOfDate"),
	GAME_OVER("gameOver"),
	INVALID("invalid");
	
	private String statusName;
	
	private ResponseStatus(String statusName){
		this.statusName = statusName;
	}
	
	public String getStatusName() {
		return statusName;
	}
	
	public static ResponseStatus fromString(String name) {
		for (ResponseStatus state : ResponseStatus.values()) {
			if (state.getStatusName().equals(name)) {
				return state;
			}
		}
		return ResponseStatus.ERROR;
	}
}
