package ru.kyune.intellect.api.json.response;

public class GetMapResponse extends ResponseGeneric {
	public String map;	
	public int whosTurn;
	public int turnNumber;
	public int timeLeft;
}
